# MiniProject1

Membuat Sticky Notes/Message yang dapat diberi comment



# Requirements

## Python version

python = 3.8.10
django =4.0.2

dapat dilakukan "pip install -r requirements.txt" untuk dependenciesnya

# Step menjalankan

1. clone atau download program pada gitlab
2. masuk ke virtualenvironment (bila diperlukan) dengan "poetry shell", atau langsung ke step ke-3
3. jalankan "docker run -p 6379:6379 -d redis:5" agar dapat dijalankan oleh multiple user
4. jalankan "python manage.py makemigrations" lalu "python manage.py migrate" untuk membuat database aplikasi
5. jalankan "python manage.py runserver"
6. buka localhost:8000/ , kemudian kita akan diminta untuk melakukan login.
7. pada localhost:8000/login, apabila kita belum memiliki akun, maka dapat melakukan register
8. setelah login selesai, pilih user yang diminta, kita juga dapat menambahkan user baru
9. pada homepage user, dapat menambahkan comment dan message pada kolom input
10. Untuk fitur chat, buka localhost:8000/chat
11. pilih nama room chat yang diinginkan
12. ruang chat terbuka, coba buka new tab dengan alamat localhost:8000/chat/<roomName> yang sama
13. lakukan chat dari kedua tab






```
Request:
    {
        "initialize": "true"
    }
```
