from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import User, Message, Comment
from .forms import CreateNewComment, CreateNewMessage
from django.contrib.auth.models import User as User2
from django.utils import timezone
# Create your views here.

def index(response):
    if response.method == "POST":
        print(response.POST)
        txt = response.POST.get("user")
        print(txt)
        user = response.POST.get("user")
        password = response.POST.get("password")
        c = User(user_text=user,password=password)
        c.save()
    all_users= User2.objects.all()
    latest_user_list = User.objects.all()
    context = {'latest_user_list': latest_user_list,'allusers': all_users}
    return render(response, 'dashboard/index.html', context)

def logins(response,user_id):
    if response.method == "POST":
        print(response.POST)
        txt = response.POST.get("user")
        print(txt)
        user = response.POST.get("user")
        password = response.POST.get("password")
        pw = User.objects.get(pk=user_id)
        if password == pw.password:
            return HttpResponseRedirect(reverse('dashboard:homepage', args=(user_id,)))
        
    latest_user_list = User.objects.all()
    context = {'latest_user_list': latest_user_list}
    return render(response, 'dashboard/logins.html', context)


def homepage(response,user_id):
    user = get_object_or_404(User, pk=user_id) 
    if response.method == "POST":
        print(response.POST)
        txt = response.POST.get("comment")
        print(txt)
        form = CreateNewComment(response.POST)
        if form.is_valid():
            Id = response.POST.get("Id")
            n = form.cleaned_data["comment"]
            c = Comment(comment_text=n,message_id=Id)
            c.save()
        else:
            Id = user_id
            n = response.POST.get("message")
            c = Message(message_text=n,user_id=Id,pub_date=timezone.now())
            c.save()
    else:
        form = CreateNewComment()
        
    return render(response, 'dashboard/homepage.html', {'user': user,'form':form})   

