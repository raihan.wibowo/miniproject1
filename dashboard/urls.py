from django.urls import path
from . import views

app_name = 'dashboard'
urlpatterns = [
    path('', views.index,name="index"),
    path('logins/<int:user_id>', views.logins,name="logins"),
    path('homepage/<int:user_id>', views.homepage,name="homepage"),
]