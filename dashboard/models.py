import datetime
from django.db import models
from django.utils import timezone

# Create your models here.
class User(models.Model):
    user_text = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    def __str__(self):
        return self.user_text

class Message(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.message_text
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

class Comment(models.Model):
    message = models.ForeignKey(Message, on_delete=models.CASCADE)
    comment_text = models.CharField(max_length=200)
    def __str__(self):
        return self.comment_text