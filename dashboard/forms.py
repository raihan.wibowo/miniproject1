from django import forms

class CreateNewComment(forms.Form):
    comment = forms.CharField(label="comment", max_length=200)

class CreateNewMessage(forms.Form):
    message = forms.CharField(label="comment", max_length=200)